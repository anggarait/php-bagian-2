
<!DOCTYPE html>
<html lang="en">

<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<title>Function</title>
</head>

<body>
<h1>Berlatih Function PHP</h1>
<?php

echo "<h3> Soal No 1 Greetings </h3>"; // SOAL NO 1

function greetings($nama){
    echo "Halo Nama saya ". $nama . "<br>";
}

greetings("Bagas");
greetings("Wahyu");
greetings("Abdul");

echo "<br>";

echo "<h3>Soal No 2 Reverse String</h3>"; // SOAL NO 2

function reverse($kata1){
    $panjang = strlen($kata1);
    $tampung = "";
    for($i = $panjang - 1; $i>=0; $i--){
        $tampung .= $kata1[$i];
    }
    return $tampung;
}
function reverseString($kata2){
    $outbal = reverse($kata2);
    echo $outbal . "<br>";
}

reverseString("abduh");
reverseString("Sanbercode");
reverseString("We Are Sanbers Developers");
echo "<br>";

echo "<h3>Soal No 3 Palindrome </h3>"; // SOAL NO 3
// Code function di sini

function palindrome($kata3){
    $balik = reverse($kata3);
    if($kata3 == $balik){
        echo $kata3 . " => Benar <br>";
    }else{
        echo $kata3 . " => Salah <br>";
    }
}

palindrome("civic") ; // true
palindrome("nababan") ; // true
palindrome("jambaban"); // false
palindrome("racecar"); // true


echo "<h3>Soal No 4 Tentukan Nilai </h3>"; // SOAL NO 4


function tentukan_nilai($angka){
    if($angka>=85 && $angka<=100){
        return $angka . " Sangat Baik <br>";
    } elseif ($angka>=70 && $angka<=84){
        return $angka . " baik <br>";
    } elseif ($angka>=40 && $angka<=69){
        return $angka . " cukup <br>";
    } elseif ($angka>=1 && $angka<=39){
        return $angka . " Kurang <br>";
    }

}

echo tentukan_nilai(98); //Sangat Baik
echo tentukan_nilai(76); //Baik
echo tentukan_nilai(67); //Cukup
echo tentukan_nilai(43); //Kurang


?>

</body>

</html>